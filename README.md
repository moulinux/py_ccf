# pyCCF
pyCCF est un gestionnaire d'évaluation écrit en Python avec le *framework* **Django**.

L'application permet de :

* Gérer la grille d'évaluation (notations);
* Gérer des étudiants;
* Évaluer les étudiants (la notion de jury est liée à l'utilisateur connecté).

Les résultats présentés tiennent compte de la grille d'évaluation de
l'épreuve **E6 du BTS SIO** où le *parcours de professionnalisation* repose
sur 16 cases et la *gestion du patrimoine* comporte 12 cases possibles
(si un étudiant est évalué très positivement, il coche donc toutes les cases).

## Installation

Pour initialiser l'application dans un environnement de développement.
> Nous supposons ci-dessous que votre environnement virtuel Django est déjà installé et activé.

Positionnez vous dans votre répertoire de travail :
```
mkdir -p $HOME/prog
cd $HOME/prog
```

Clonez le projet :
```
git clone git@framagit.org:moulinux/py_ccf.git
```

Initialisez la structure de la base de données (sqlite par défaut) :
```
cd py_amap
python manage.py migrate
```

Créez le premier utilisateur, qui sera également le compte du webmestre :
```
python manage.py createsuperuser
```

Initialisez la grille d'évaluation dans la base de données :
```
python db_init.py
```

Lancez le serveur de développement :
```
python manage.py runserver
```
