from app_eval.models import Notation

n1 = Notation(libelle='Très insuffisant')
n2 = Notation(libelle='Insuffisant')
n3 = Notation(libelle='Bien')
n4 = Notation(libelle='Très bien')
n1.save()
n2.save()
n3.save()
n4.save()
