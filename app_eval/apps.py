from django.apps import AppConfig


class AppEvalConfig(AppConfig):
    name = 'app_eval'
