from django.db import models
from django.contrib.auth.models import User


class Notation(models.Model):
    """Définition d'une Notation"""

    libelle = models.CharField(max_length=20)

    class Meta:
        db_table = 'notations'

    def __str__(self):
        """
        Donne une représentation textuelle d'une notation
        """
        return self.libelle


class Etudiant(models.Model):
    """Définition d'un Etudiant"""

    nom = models.CharField(max_length=20)
    prenom = models.CharField(max_length=20)

    class Meta:
        db_table = 'etudiants'

    def __str__(self):
        """
        Donne une représentation textuelle d'un étudiant
        """
        return "{} {}".format(self.prenom, self.nom)


class Evaluation(models.Model):
    """Définition d'une Evaluation"""

    jury = models.ForeignKey(User, on_delete=models.CASCADE)
    etudiant = models.ForeignKey(Etudiant, on_delete=models.CASCADE)
    qualite = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                related_name='ref_qualite',
                                verbose_name='Qualité de la présentation')
    pertinence = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                   related_name='ref_pertinence',
                                   verbose_name='Pertinence analyse critique')
    variete = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                related_name='ref_variete',
                                verbose_name='Variété des SP rencontrées')
    maitrise_gen = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                     related_name='ref_maitrise_gen',
                                     verbose_name='Degré de maîtrise générale')
    gestion_config = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                       related_name='ref_gestion_config',
                                       verbose_name='Pertinence usage dconfig')
    veille_techno = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                      related_name='ref_veille_techno',
                                      verbose_name='Veille techno')
    maitrise_pat = models.ForeignKey(Notation, on_delete=models.CASCADE,
                                     related_name='ref_maitrise_pat',
                                     verbose_name='Maîtrise gestion patri.')

    class Meta:
        db_table = 'evaluations'
        unique_together = (("jury", "etudiant"),)

    def __str__(self):
        """
        Donne une représentation textuelle d'une évaluation
        """
        return "Evaluation {} | {}".format(self.etudiant, self.jury)
