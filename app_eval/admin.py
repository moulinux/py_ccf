from django.contrib import admin
from app_eval.models import Notation, Etudiant, Evaluation


class EvaluationAdmin(admin.ModelAdmin):
    def get_changeform_initial_data(self, request):
        get_data = super(EvaluationAdmin, self).\
            get_changeform_initial_data(request)
        get_data['jury'] = request.user.pk
        return get_data


admin.site.register(Notation)
admin.site.register(Etudiant)
admin.site.register(Evaluation, EvaluationAdmin)
admin.site.site_title = 'Administration CCF'
admin.site.site_header = 'Administration CCF'
