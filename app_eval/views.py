from django.shortcuts import render
from app_eval.models import Notation, Etudiant, Evaluation


def index(request):
    return render(None, 'accueil.html')


def resultats(request):
    resultats = []
    qs_etudiants = Etudiant.objects.all()
    for etudiant in qs_etudiants:
        # Le traitement fait la moyenne de plusieurs évaluations d'un étudiant
        # (faîtes par plusieurs jurys).
        qs_evaluations = Evaluation.objects.all().filter(etudiant=etudiant.id)
        note_parcours = 0
        note_patrimoine = 0
        nb_eval = 0
        for eval in qs_evaluations:
            nb_eval += 1
            # on comptabilise le nombre de cases à partir de l'identifiant
            # Très insuffisant: 1 case; ..; Très bien: 4 cases "cochés"
            note_parcours += (eval.qualite.id + eval.pertinence.id +
                              eval.variete.id + eval.maitrise_gen.id)*(10/16)
            note_patrimoine += (eval.gestion_config.id + eval.veille_techno.id
                                + eval.maitrise_pat.id)*(10/12)
        if note_parcours != 0:
            note_parcours = note_parcours / nb_eval
            note_patrimoine = note_patrimoine / nb_eval
        fiche = {}
        fiche['nom'] = etudiant.nom
        fiche['prenom'] = etudiant.prenom
        fiche['note_parcours'] = note_parcours
        fiche['note_patrimoine'] = note_patrimoine
        fiche['note_final'] = note_parcours + note_patrimoine
        resultats.append(fiche)

    return render(None, 'resultats.html', {'resultats': resultats, })
